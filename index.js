const express = require('express')
const nodemailer = require('nodemailer');
const pug = require('pug');
const app = express()

app.use(express.static('styles.css'))


const transporter = nodemailer.createTransport({
   service: 'gmail',
   auth: {
      user: 'estebangular.aragon@gmail.com',
      pass: 'webteban'
   }
});

const myTemplate = pug.compileFile('index.pug');
// myTemplate({friendName: "foo", Name: "Bar"});

app.get('/', (req, res) => {
   const mailOptions = {
      from: 'estebangular.aragon@gmail.com',
      to: 'esteban.aragonm@gmail.com',
      subject: 'Sending Email using Node.js',
      text: 'That was easy!',
      html: myTemplate()
   };

   transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
         console.log(error);
      } else {
         res.status(200).send(info.response)
      }
   });

})

app.listen(3000, () => console.log('Example app listening on port 3000!'))